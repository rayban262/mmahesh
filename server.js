// Import modules and external files
const express = require("express");
const api = require("./api");

// Initialise Express app
const app = express();

// Declare routes to be used and by which router
app.use("/api", api);

// Setup the port
const port = process.env.PORT || 5000;
app.listen(port, () => console.log(`Server running on port ${port}`));
