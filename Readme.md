<h1>Readme</h1>
<h2>Requirements</h2>
<ul>
<li>To create a Single Page Application</li>
<li>To use any JS framework or vanilla JS (I've used React)</li>
<li>Readme.md</li>
</ul>

<h2>Tasks</h2>
<ul>
<li>Build a dashboard to represent data provided visually</li>
<li>Use JSON files provided as data source and <strong>may</strong> wish to write as though I were retrieving it from an api endpoint</li>
<li>Exist as widgets that can move around <strong>or</strong> hidden from the dashboard
<li>Use any third party libraries that I deem fit</li>
</ul>

<h2>3rd Party Libraries utilised</h2>
<ul>
<li>Express: The backend/server to supply the json to the react client</li>
<li>Axios: To send the get request to the express server</li>
<li>Bootstrap + Reactstrap: To style the page</li>
<li>Nodemon(Dev dependency): To automatically rerun the server when any changes are made </li>
<li>Concurrently(Dev dependency): To run the frontend and backend concurrently with one line of code</li>
</ul>

<h2>To run the Application</h2>
<p>Open terminal and enter the project directory. Next, run any of the following scripts:</p>
<ul>
<li>Frontend + Backend: <code>npm run dev</code></li>
<li>Backend ONLY: <code>npm run server</code></li>
<li>Frontend ONLY: <code>npm run client</code></li>
</ul>

<h2>Using the Application</h2>
<ol>
<li>Start the application in terminal by entering into the project folder and running the script <code>npm run dev</code>
<li>Checkboxes for the widgets will be visible on top of the page. Checking the boxes will show/hide the respective widget and more than one box can be checked at a time</li>
<li>Checking a box will show the widget and the data will be displayed in a table form</li>
</ol>
<p>Note: Scroll vertically in the languages card to see the rest of the language data</p>

<h2>Key Challenges Encountered</h2>
<ul>
<li>Using the local json file: I thought I wouldn't need a backend and through react I would be able to directly parse the json files. Turns out it's not so easy. Also, fetch() and axios() both accept only URLs as parameters, so relative paths/file paths would not work. So using an express server, I was able to send the json request</li>
<li>Running Server+React concurrently: I ran the frontend and backend on different ports and added the server port as a proxy to the react client</li>
<li>I don't know how to implement the ability to drag/move the widgets around the page. So I only implemented the ability to show/hide the widgets</li>
<li>I haven't yet learnt how to include tests. Was one of the items on my list of things to learn :)</li>
</ul>
<p>The rest of the stuff was fine</p>

<h2>Questions I still have</h2>
<p>What do I use the labels json files for? Maybe I might have missed something. Because all the data in the labels file are also available in the data files. For doctypes, the labels have the id number as well, but we can simply use the index instead of "getting" data from another json file</p>
