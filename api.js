// Import modules and external files
const express = require("express");
const confidentialityData = require("./source_data/confidentiality_data.json");
const doctypeData = require("./source_data/doctype_data.json");
const doctypeLabels = require("./source_data/doctype_labels.json");
const languageData = require("./source_data/language_data.json");

// Initialise router
const router = express.Router();

// Initialise backend routes
router.get("/test", (req, res) => res.json({ msg: "Users Works" }));

// Confidentiality Data
router.get("/confidentiality_data", (req, res) => {
  res.json(confidentialityData);
});

// Doctype Data
router.get("/doctype_data", (req, res) => {
  res.json(doctypeData);
});

// Language Data
router.get("/language_data", (req, res) => {
  res.json(languageData);
});

module.exports = router;
